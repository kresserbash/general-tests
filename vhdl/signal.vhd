Library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;


--	  +--------+
--	->|clk	  q|->
--	->|rst	   |
--	->|clr	   |
--	->|enb	   |
--	->|load	   |
--	->|ld	   |
-- 	  +--------+
--
--	load: 0000
--	q: 0000
--

Entity counters is
port(
	input : in std_logic_vector (3 downto 0);
	clock : in std_logic;
	output : out std_logic;
end entity;

architecture ex_a_for of counters is
	variable count := 0;
	variable power := 0;

	begin 
	count <= 0;
	for i in 0 to 3 loop
		power <= 2 ** power;
		if input(i) = '1'
			count <= count + power;
		end if;
	end loop;

	output <= count;
End architecture;

architecture ex_b_while of counters is
	signal count : std_logic_vector (3 downto 0);
	variable power := 0;

	begin 
	count <= 0;
	while power < 8
		power <= 2 ** power;
		if input(i) = '1'
			count <= count + power;
		end if;
	end loop;

	output <= count;
end architecture;
architecture ex_a_signals of counters is
	signal count : std_logic_vector (3 downto 0);
	variable power := 0;

	begin 
	count <= 0;
	case tries is
		when tries = 0 =>
			power <= 2 ** power;
			count <= count + 1;
		when input(1) =>
			count <= count + 2;
		when input(2) =>
			count <= count + 4;
		when input(3) =>
			count <= count + 8;
	end case;
	case input is
		when input(0) =>
			count <= count + 1;
		when input(1) =>
			count <= count + 2;
		when input(2) =>
			count <= count + 4;
		when input(3) =>
			count <= count + 8;
	end case;
	case input is
		when input(0) =>
			count <= count + 1;
		when input(1) =>
			count <= count + 2;
		when input(2) =>
			count <= count + 4;
		when input(3) =>
			count <= count + 8;
	end case;
	case input is
		when input(0) =>
			count <= count + 1;
		when input(1) =>
			count <= count + 2;
		when input(2) =>
			count <= count + 4;
		when input(3) =>
			count <= count + 8;
	end case;
end architecture;

/*
/*Primeiro programa assembly. Como Hello World, vou acender o led da placa
/*As portas B do micro tem 8 bits de de informação bi-direcional(I/O), com resistores pull-up
/*		cada qual para uma porta, variantes de PB0 à PB7.
/*0b00000000	Os registradores (R0...R31) São contados na memória , sequencialmente (0x00...0x1F),
/*		sendo os 6 ultimos 2 registradores X's, 2 Y's e 2 Z's: endereçados com 16 endereços
/*para endereçar espaço das informações.
/*A porta é escolhida determinando uma letra(B,C ou D), e um número(posicional do bit) dentro dos 8
/*bits da porta: PORTB5 indica o 5º bit do endereço PORTB.
/*Para cada porta, três endereços de memória são alocados: PORTx, DDRx e PINx, respectivamente:
/*O registrador de dados, registrador da direção e o pino de entrada. Por serem informações constantes
/*os endereços de pino de entrada são para apenas leitura. Não se modifica seus endereços.
/*
/*O nome dos bits de DDRx são DDxn, sendo x e n os indicadores da letra e do numeral da porta. Se alto
/*a porta é saída, e se baixo, a porta é entrada.
/*O nome dos endereços de PORTx são PORTxn. Quando em modo saída(0 em DDxn), alto e baixo ativam e desativam o
/*resistor pull-up. Quando em modo entrada, alto e baixo ligam e desligam a tensão no pino.
/*O nome dos endereços de PINx são PINxn, e sua função é alterar o estado da porta, independente do valor em DDxn
/*A ordem de precedência, na memória, é: PINx, DDRx, PORTx. A começa em 0x00, B começa em 0x03...
/*
/*
/*
/*
/*
*/


#include <avr/io.h>

.set PORTB, 0x05	;Selecionamos o quinto bit das portas B, correspondente a PB5
.set DDRB, 0x04		;Selecionamos o quinto bit de IO das portas B, correspondente a PB5
;.set PINB, 0x03
.set LED_M, 0b0000000	;Definimos um estado para todas as portas, sendo o 5º bit alto, para ligar o led nessa porta

.org	0x0000	;Início de memória.
	;ldi r16, LED_M	;Carregando o valor da macro do led
	sbi DDRB, 5	;Definimos o pino como sendo saída, setando apenas o 5º bit da seq.


infinity:
	sbi PORTB, 5
	rcall delay_1s
	cbi PORTB, 5
	rcall delay_1s
	rjmp infinity	;Then to infinity, we go!

/*
/*A stack é uma área da memória que guarda as operações de um programa
/* e funciona na estrutura LIFO, ou seja, é possível usar temporariame-
/*nete como uma variável de sistema, e depois remover o ultimo valor u-
/*sado.
/*
/*A conta de delay é simples. O clock base do micro é de 1MHz, logo, o
/*período é de 1*10^(-6)s. Multiplicando por 10^6, temos o numero de
/*ciclos necessários para gerar um delay de 1s
*/

delay_1s:	;Wrapper
	rcall delay_500ms
	rcall delay_500ms
	ret
delay_500ms:	;Wrapper
	rcall delay_100ms
	rcall delay_100ms
	rcall delay_100ms
	rcall delay_100ms
	rcall delay_100ms
	rcall delay_100ms
	rcall delay_100ms
	rcall delay_100ms
	rcall delay_100ms
	rcall delay_100ms
	ret
delay_100ms:	;Wrapper
	rcall delay_10ms
	rcall delay_10ms
	rcall delay_10ms
	rcall delay_10ms
	rcall delay_10ms
	rcall delay_10ms
	rcall delay_10ms
	rcall delay_10ms
	rcall delay_10ms
	rcall delay_10ms
	ret
delay_10ms:	;Wrapper
	rcall delay_1ms
	rcall delay_1ms
	rcall delay_1ms
	rcall delay_1ms
	rcall delay_1ms
	rcall delay_1ms
	rcall delay_1ms
	rcall delay_1ms
	rcall delay_1ms
	rcall delay_1ms
	ret
delay_1ms:	;Wrapper
	rcall delay_1us
	rcall delay_1us
	rcall delay_1us
	rcall delay_1us
	rcall delay_1us
	rcall delay_1us
	rcall delay_1us
	rcall delay_1us
	rcall delay_1us
	rcall delay_1us
	ret
delay_1us:		;Fazemos 10 operações, para contar um microssegundo
	push r16	;Salvando o conteúdo de r16 no stack. O stack é decrementado logo em seguida
	ldi r16, 99	;Carrega 99 para servir de contador.
	delay_1us1:
		nop		;Fazer nada. Sentar e esperar, 5 vezes
		nop		;
		nop		;
		nop		;
		nop		;
		dec r16		;Decrementamos 1 do valor de r16
		brne delay_1us1	;Ramificamos a função de delay caso r16 não seja 0. Gasta 1 ciclo
	pop r16		;Recupera o valor original de r16 do stack. Gasta 2 ciclos.
	ret		;

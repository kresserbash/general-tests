/*
Este programa escreve o estado de um botão diretamente num circuito com led. Apertou, o led acende.
Soltou, o led apaga
*/

#include <avr/io.h>

.set PINB, 0x03
.set DDRB, 0x04
.set PORTB, 0x05
.set PINC, 0x06
.set DDRC, 0x07
.set PORTC, 0x08

/*
Para fins de facilitar, ao referirmos saída e entrada, estamos falando do FLUXO PARTINDO DO MICRO:
Saída na porta x significa que o micro envia dados à esta porta.
Entrada na porta x significa que o micro lê estados nessa porta.

Por padrão, todas os bits DDRX estão em estado baixo(entrada de dados).
Para transformar uma porta em saída, setamos os valores ddrx como 1. Se for entrada, é 0
Por exemplo, ler um botão na porta D12 e escrever o estado no led (porta D8) requer que, sendo
o bit PB1 equivalente à porta 8, este esteja em 0(entrada dos dados de um botão). O bit PB0, equi-
valente à porta 13 esteja em 1(escrita do estado no led).

*/

.set LED_M, 0b000001		;//Ligando o led na porta D8
/*
Cuidado aqui. Caso uma porta esteja setado para entrada, e você escreva alto nela(como se fosse porta de saída),
o resistor pull_up é ativado, cujo objetivo é impedir a saída de dados na porta que não está sendo usada
(como que para manter ela REALMENTE desativada, sem tensão nem corrente, numa forma idealizada).
*/

.set PSTATE, 0b01		;//O estado das portas: porta PB1 como entrada (botão), PB0 como saída(led).

.org 0x00
	out DDRB, PSTATE	;//Configurando entrada e saída de dados.
	nop			;//A mudança de estado entrada/saída das portas leva um tempo. Vamos esperar
	rcall loop

loop:
	rcall read
	rcall write
	rjmp loop

read:
	in r16, PINB		;//Lemos o estado da portabX, e escrevemos no r16 (r0-r15 não fazem operaçoes diretas)
/*
É importante citar que a linha acima faz a leitura de TODOS os bits dos endereços de PINB. A próxima linha vai isolar
apenas os bits que nos interessam, que é o bit em que o botão está ligado.
*/
	andi r16, 0b10		;//Essa operação força que apenas o bit botão possua um estado 1. Se ele for 1, o andi
				;//resulta em 1 para essa porta apenas(o resto dá resultado 0)
ret

write:
	asr r16		;//deslocando bit do botão para direita
/*
Uma traquinagem aqui. As portas botão e led são vizinhas na memória para facilitar operações de liga e desliga: usando
o asr (arithmetic shift right), deslocamos o bit (do botão) para a direita (bit do led), fazendo ele ligar(ou não)
usando apenas 1 ciclo. Sem operações desnecessárias.
*/
	out PORTB, r16		;//Como isolamos o bit do botão em read, não precisamos nos preocupar com leituras
				;//passadas. Os bits são limpos toda vez que lemos o botão.
ret

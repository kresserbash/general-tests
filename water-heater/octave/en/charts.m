1;

function chart1(exp_data, g1_data, g2_data, n)
	global USEAVERAGE;

	figure;
	plot(1:n, exp_data, "r;Experimental;", 1:n+1, g1_data, "g;Theorical 1;", 1:n+1, g2_data, "b;Theorical 2;");
	xlabel ("time (s)");
	ylabel ("temp (ºC)");
	legend("location", "southeast");
	if USEAVERAGE
		title('CHART 2: Models comparsion: Practical(aprox.) and theorical');
		print -dpng "2-models-comparsion-avg.png"
	else
		title('CHART 1: Models comparsion: Practical and theorical');
		print -dpng "1-models-comparsion.png"
	endif
endfunction

function chart2(exp_data, n)
	global USEAVERAGE;

	figure;
	plot(1:n, exp_data, "r;Experimental;");
	xlabel ("time (s)");
	ylabel ("temp (ºC)");
	legend("location", "southeast");
	if USEAVERAGE
		title('CHART 4: Experimental, (aprox.), model');
		print -dpng "4-experimental-model-avg.png"
	else
		title('CHART 3: Experimental model');
		print -dpng "3-experimental-model.png"
	endif
endfunction

function chart3(g1_data, n)
	figure;
	plot(1:n, g1_data(1:n), "g;Theorical 1;");
	title('CHART 5: Theorical model 1');
	xlabel ("time (s)");
	ylabel ("temp (ºC)");
	legend("location", "southeast");
	print -dpng "5-theorical-1-model.png"
endfunction

function chart4(g2_data, n)
	figure;
	plot(1:n, g2_data(1:n), "b;Theorical 2;");
	title('CHART 6: Theorical model 2');
	xlabel ("time (s)");
	ylabel ("temp (ºC)");
	legend("location", "southeast");
	print -dpng "6-theorical-2-model.png"
endfunction

function chart5(exp_data, avgexp_data, n)
	interval=n/16;
	figure;
	plot(1:interval, exp_data(1:interval), "r;Original;", 1:interval, avgexp_data(1:interval), "m;Aproximated;");
	title('CHART 7: Comparsion between experimental and aproximated function');
	xlabel ("time (s)");
	ylabel ("T (ºC)");
	legend("location", "southeast");
	print -dpng "7-experimental-aproximated-comparsion.png"
endfunction

function chart6(delta1, delta2, n)
	global USEAVERAGE;

	figure;
	plot(1:n, delta1, "g;|G1-exp|;", 1:n, delta2, "b;|G2-exp|;");
	xlabel ("time (s)");
	ylabel ("d(dT) (ºC)");
	legend("location", "southeast");
	if USEAVERAGE
		title('CHART 9: System difference (aprox.) for G1 and G2');
		print -dpng "9-system-instant-error-avg.png"
	else
		title('CHART 8: System difference for G1 and G2');
		print -dpng "8-system-instant-error-avg.png"
	endif
endfunction

function chart7(integ1, integ2, n)	
	global USEAVERAGE;

	figure;
	plot(1:n, integ1, "g;integral (|G1-exp|);", 1:n, integ2, "b;integral (|G2-exp|);");
	xlabel ("time (s)");
	ylabel ("d(dT) (ºC)");
	legend("location", "southeast");
	if USEAVERAGE
		title('CHART 11: Integral of system difference (aprox.) for G1 and G2');
		print -dpng "11-system-cumulated-error-avg.png"
	else
		title('CHART 10: Integral of system difference for G1 and G2');
		print -dpng "10-system-cumulated-error.png"
	endif
endfunction


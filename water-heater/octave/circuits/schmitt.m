# According to Pertence Junior, p107
# With positive feedback,
#	R1 does not matter for the output
#	Positive sat (Vi negative)
#		Vds = -Vsat*R2/(R2+R3)
#	Negative sat (Vo positive)
#		Vdi = Vsat*R2/(R2+R3)
#	and Vsat = Vcc - 1.5V
#
# Note that the the output will be inverted in this case: Negative Vi
# implies in positive Vout
#
# Vds
#
1;

global VCC = 15;
global RPATTERN = 10*10^3;

function [R1, R2] = schmitt_positive(VCTR, R1)
	global VCC;
	VSAT = VCC-1.5;
	R1 = R1;
	R2 = R1*(VSAT/VCTR-1);
endfunction

function [R1, R2] = schmitt_negative(VCTR, R1)
	global VCC;
	VSAT = VCC-1.5;
	R1 = R1;
	R2 = R1*VSAT/VCTR;
endfunction



#TODO: Rewrite this as it must be independent from schmitt
# As it is in 

#TODO: Make for an arbritary number of given parameters

1;

function R2 = gen_integrator(VPP, f, R1, C1)
# I can't do nothing yet;
	R2 = R1+1;
endfunction

# According to Malvino, p875
# UTP = R1*Vsat/R2, so R2 = R1*Vsat/UTP
# as 2*UTP = VPP (Wanted triangular peak to peak voltage)
#
#  R2 = R2*2*Vsat/VPP
#
# f = R2/(4*R1*R3*C)
#
#
# p 875
# R4 >= 10 R3
#
#

function [C1, R1, R2, R3, R4] = gen_triangular(VPP, f, R1, C)
	global VCC;
	VSAT = VCC-1.5;
	C1 = C;
	R1 = R1;
	R2 = R1*2*VSAT/VPP;
	R3 = R2/(4*R1*f*C);
	R4 = 10*R3;
endfunction

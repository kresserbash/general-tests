# Trabalho 2: Integração de habilidades - 2022/1

Disciplina: Redes de Computadores
Curso: Engenharia de Computação

Nome: Allan de Mello Ribeiro
RA: 2037807

## Tarefa 1:Sub-Redes

|Sub-Rede	|IPv6 				|IPv4	 	|Máscara		|Broadcast	| 
|---------------|-------------------------------|---------------|-----------------------|---------------|
|Matriz 	|2001:DB8:ACAD:700::/64		|200.200.7.0	|255.255.255.192	|200.200.7.63	|
|Filial 1 	|2001:DB8:ACAD:701::/64		|200.200.7.64 	|255.255.255.224	|200.200.7.95	|
|Filial 2 	|2001:DB8:ACAD:702::/64		|200.200.7.96 	|255.255.255.224	|200.200.7.127	|
|Filial 3 	|2001:DB8:ACAD:703::/64		|200.200.7.128	|255.255.255.224	|200.200.7.159	|
|Filial 4 	|2001:DB8:ACAD:704::/64		|200.200.7.160	|255.255.255.224	|200.200.7.192	|
|Filial 5 	|2001:DB8:ACAD:705::/64		|200.200.7.192	|255.255.255.224	|200.200.7.223	|
|pb-vit 	|2001:DB8:ACAD:7FF::0:0/112	|200.200.7.224	|255.255.255.252	|200.200.7.227	|
|vit-fb 	|2001:DB8:ACAD:7FF::1:0/112	|200.200.7.228	|255.255.255.252	|200.200.7.231	|
|fb-ita 	|2001:DB8:ACAD:7FF::2:0/112	|200.200.7.232	|255.255.255.252	|200.200.7.235	|
|ita-pb 	|2001:DB8:ACAD:7FF::3:0/112	|200.200.7.236	|255.255.255.252	|200.200.7.239	|
|cv-ita 	|2001:DB8:ACAD:7FF::4:0/112 	|200.200.7.240	|255.255.255.252	|200.200.7.243	|


## Tarefa 2: Endereçamento de Dispositivos

|Dispositivo	|Interf.|IPv4		|Máscara		|Gateway(IPV4)	|IPv6/Prefixo (GUA)		|IPV6 (LLA)	|Gateway(IPV6)		|
|---------------|-------|---------------|-----------------------|---------------|-------------------------------|---------------|-----------------------|
|PC1		|NIC	|200.200.7.3	|255.255.255.192	|200.200.7.1	|2001:DB8:ACAD:700::3/64	|EUI-64		|2001:DB8:ACAD:700::1/64|
|PC2		|NIC	|200.200.7.4	|255.255.255.192	|200.200.7.1	|2001:DB8:ACAD:700::4/64	|EUI-64		|2001:DB8:ACAD:700::1/64|
|PC3		|NIC	|200.200.7.67	|255.255.255.224	|200.200.7.65	|2001:DB8:ACAD:701::3/64	|EUI-64		|2001:DB8:ACAD:701::1/64|
|PC4		|NIC	|200.200.7.68	|255.255.255.224	|200.200.7.65	|2001:DB8:ACAD:701::4/64	|EUI-64		|2001:DB8:ACAD:701::1/64|
|PC5		|NIC	|200.200.7.99	|255.255.255.224	|200.200.7.97	|2001:DB8:ACAD:702::3/64	|EUI-64		|2001:DB8:ACAD:702::1/64|
|PC6		|NIC	|200.200.7.100	|255.255.255.224	|200.200.7.97	|2001:DB8:ACAD:702::4/64	|EUI-64		|2001:DB8:ACAD:702::1/64|
|Switch-Matriz	|SVI	|200.200.7.2	|255.255.255.192	|200.200.7.1	|X				|X		|X			|
|Switch-Filial1 |SVI	|200.200.7.66	|255.255.255.224	|200.200.7.65	|X				|X		|X			|
|Switch-Filial2 |SVI	|200.200.7.98	|255.255.255.224	|200.200.7.97	|X				|X		|X			|
|Rot. P.B. 	|Fa0/0	|200.200.7.1	|255.255.255.192	|X		|2001:DB8:ACAD:700::1/64	|FE80::1	|X			|
|Rot. P.B. 	|Se0/0/0|200.200.7.225	|255.255.255.252	|X		|2001:DB8:ACAD:7FF::1/112	|EUI-64		|X			|
|Rot. P.B. 	|Se0/0/1|200.200.7.238	|255.255.255.252	|X		|2001:DB8:ACAD:7FF::3:2/112	|EUI-64		|X			|
|Rot. F.B.	|Fa0/0	|200.200.7.65	|255.255.255.224	|X	 	|2001:DB8:ACAD:701::1/64	|FE80::1	|X			|
|Rot. F.B.	|Se0/0/0|200.200.7.233	|255.255.255.252	|X		|2001:DB8:ACAD:7FF::2:1/112	|EUI-64		|X			|
|Rot. F.B.	|Se0/0/1|200.200.7.230	|255.255.255.252	|X		|2001:DB8:ACAD:7FF::1:2/112	|EUI-64		|X			|
|Rot. Vit.	|Se0/0/0|200.200.7.229	|255.255.255.252	|X		|2001:DB8:ACAD:7FF::1:1/112	|EUI-64		|X			|
|Rot. Vit.	|Se0/0/1|200.200.7.226	|255.255.255.252	|X		|2001:DB8:ACAD:7FF::2/112	|EUI-64		|X			|
|Rot. Itap. 	|Se0/0/0|200.200.7.237	|255.255.255.252	|X		|2001:DB8:ACAD:7FF::3:1/112	|EUI-64		|X			|
|Rot. Itap.	|Se0/0/1|200.200.7.234	|255.255.255.252	|X		|2001:DB8:ACAD:7FF::2:2/112	|EUI-64		|X			|
|Rot. Itap. 	|Fa0/1	|200.200.7.241	|255.255.255.252	|X		|2001:DB8:ACAD:7FF::4:1/112	|EUI-64		|X			|
|Rot. Cor. 	|Fa0/0	|200.200.7.97	|255.255.255.224	|X	 	|2001:DB8:ACAD:702::1/64	|FE80::1	|X			|
|Rot. Cor. 	|Fa0/1	|200.200.7.242	|255.255.255.252	|X		|2001:DB8:ACAD:7FF::4:2/112	|EUI-64		|X			|

## Tarefa 3: Tabela de Roteamento

### Roteador Pato Branco (Rot. P.B.)	

#### IPv4

|Destino		|Máscara		|Next Hop	|Saída		|
|-----------------------|-----------------------|---------------|---------------|
|200.200.7.228		|255.255.255.252	|200.200.7.226	|Se0/0/0	|
|200.200.7.232		|255.255.255.252	|200.200.7.226	|Se0/0/0	|
|200.200.7.64		|255.255.255.224	|200.200.7.226	|Se0/0/0	|
|200.200.7.240		|255.255.255.252	|200.200.7.226	|Se0/0/0	|
|200.200.7.96		|255.255.255.224	|200.200.7.226	|Se0/0/0	|

#### IPv6

|Destino/Prefixo		|Next Hop	|Saída	|
|-------------------------------|-----------------------|---------------|
|2001:DB8:ACAD:7FF::1:0/112	|2001:DB8:ACAD:7FF::2	|Se0/0/0	|
|2001:DB8:ACAD:7FF::2:0/112	|2001:DB8:ACAD:7FF::2	|Se0/0/0	|
|2001:DB8:ACAD:701::0/64	|2001:DB8:ACAD:7FF::2	|Se0/0/0	|
|2001:DB8:ACAD:7FF::4:0/112	|2001:DB8:ACAD:7FF::2	|Se0/0/0	|
|2001:DB8:ACAD:702::0/64	|2001:DB8:ACAD:7FF::2	|Se0/0/0	|

### Roteador Vitorino (Rot. Vit.)

#### IPv4

|Destino	|Máscara		|Next Hop	|Saída		|
|---------------|-----------------------|---------------|---------------|
|200.200.7.64 	|255.255.255.224	|200.200.7.230	|Se0/0/0	|
|200.200.7.232 	|255.255.255.252	|200.200.7.230	|Se0/0/0	|
|200.200.7.240 	|255.255.255.252	|200.200.7.230	|Se0/0/0	|
|200.200.7.96 	|255.255.255.224	|200.200.7.230	|Se0/0/0	|
|200.200.7.236	|255.255.255.252	|200.200.7.230	|Se0/0/0	|

#### IPv6

|Destino/Prefixo		|Next Hop		|Saída		|
|-------------------------------|-----------------------|---------------|
|2001:DB8:ACAD:701::/64		|2001:DB8:ACAD:7FF::1:2	|Se0/0/0	|
|2001:DB8:ACAD:7FF::2:0/112	|2001:DB8:ACAD:7FF::1:2	|Se0/0/0	|
|2001:DB8:ACAD:7FF::4:0/112	|2001:DB8:ACAD:7FF::1:2	|Se0/0/0	|
|2001:DB8:ACAD:702::/64		|2001:DB8:ACAD:7FF::1:2	|Se0/0/0	|
|2001:DB8:ACAD:7FF::3:0/112	|2001:DB8:ACAD:7FF::1:2	|Se0/0/0	|

### Roteador Francisco Beltrão (Rot. F.B.)	

#### IPv4

|Destino	|Máscara		|Next Hop	|Saída		|
|---------------|-----------------------|---------------|---------------|
|200.200.7.96 	|255.255.255.224	|200.200.7.234	|Se0/0/0	|
|200.200.7.240 	|255.255.255.252	|200.200.7.234	|Se0/0/0	|
|200.200.7.236 	|255.255.255.252	|200.200.7.234	|Se0/0/0	|
|200.200.7.224 	|255.255.255.252	|200.200.7.234	|Se0/0/0	|
|200.200.7.0 	|255.255.255.192	|200.200.7.234	|Se0/0/0	|
#### IPv6
|Destino/Prefixo		|Next Hop		|Saída		|
|-------------------------------|-----------------------|---------------|
|2001:DB8:ACAD:702::/64		|2001:DB8:ACAD:7FF::2:2	|Se0/0/0	|
|2001:DB8:ACAD:7FF::4:0/112	|2001:DB8:ACAD:7FF::2:2	|Se0/0/0	|
|2001:DB8:ACAD:7FF::3:0/112	|2001:DB8:ACAD:7FF::2:2	|Se0/0/0	|
|2001:DB8:ACAD:7FF::/112	|2001:DB8:ACAD:7FF::2:2	|Se0/0/0	|
|2001:DB8:ACAD:700::/64		|2001:DB8:ACAD:7FF::2:2	|Se0/0/0	|

### Roteador Itapejara D'Oeste (Rot. Itap.) 

#### IPv4

|Destino	|Máscara		|Next Hop	|Saída		|
|---------------|-----------------------|---------------|---------------|
|200.200.7.0 	|255.255.255.192	|200.200.7.238	|Se0/0/0	|
|200.200.7.224 	|255.255.255.252	|200.200.7.238	|Se0/0/0	|
|200.200.7.228 	|255.255.255.252	|200.200.7.238	|Se0/0/0	|
|200.200.7.64 	|255.255.255.224	|200.200.7.238	|Se0/0/0	|
|200.200.7.96 	|255.255.255.224	|200.200.7.242	|Fa0/1		|

#### IPv6

|Destino/Prefixo		|Next Hop		|Saída		|
|-------------------------------|-----------------------|---------------|
|2001:DB8:ACAD:700::/64		|2001:DB8:ACAD:7FF::3:2	|Se0/0/0	|
|2001:DB8:ACAD:7FF::/112	|2001:DB8:ACAD:7FF::3:2	|Se0/0/0	|
|2001:DB8:ACAD:7FF::1:0/112	|2001:DB8:ACAD:7FF::3:2	|Se0/0/0	|
|2001:DB8:ACAD:701::/64		|2001:DB8:ACAD:7FF::3:2	|Se0/0/0	|
|2001:DB8:ACAD:702::/64		|2001:DB8:ACAD:7FF::4:2	|Fa0/1		|


### Roteador Coronel Vivida (Rot. Cor.)

#### IPv4

|Destino	|Máscara		|Next Hop	|Saída	|
|---------------|-----------------------|---------------|-------|
|200.200.7.0 	|255.255.255.192	|200.200.7.241	|Fa0/1	|
|200.200.7.64 	|255.255.255.224	|200.200.7.241	|Fa0/1	|
|200.200.7.236 	|255.255.255.252	|200.200.7.241	|Fa0/1	|
|200.200.7.228 	|255.255.255.252	|200.200.7.241	|Fa0/1	|
|200.200.7.232 	|255.255.255.252	|200.200.7.241	|Fa0/1	|

#### IPv6

|Destino/Prefixo		|Next Hop		|Saída	|
|-------------------------------|-----------------------|-------|
|2001:DB8:ACAD:700::/64		|2001:DB8:ACAD:7FF::4:1	|Fa0/1	|
|2001:DB8:ACAD:701::/64		|2001:DB8:ACAD:7FF::4:1	|Fa0/1	|
|2001:DB8:ACAD:7FF::3:0/112	|2001:DB8:ACAD:7FF::4:1	|Fa0/1	|
|2001:DB8:ACAD:7FF::/112	|2001:DB8:ACAD:7FF::4:1	|Fa0/1	|
|2001:DB8:ACAD:7FF::1:0/112	|2001:DB8:ACAD:7FF::4:1	|Fa0/1	|
|2001:DB8:ACAD:7FF::2:0/112	|2001:DB8:ACAD:7FF::4:1	|Fa0/1	|


## Topologia - Packet Tracer

- [ ] ![Trabalho2-Topologia-AllanRibeiro](trabalho2-topologia-AllanRibeiro.pkt)


## Arquivos de Configuração dos Dispositivos Intermediários (roteadores e switches)

- [ ] ![Roteador Pato Branco](r-pb-nnn.txt)
- [ ] ![Roteador Francisco Beltrão](r-fb-nnn.txt)
- [ ] ![Roteador Vitorino](r-vit-nnn.txt)
- [ ] ![Roteador Itapejara D'Oeste](r-ita-nnn.txt)
- [ ] ![Roteador Coronel Vivida](r-cv-nnn.txt)
- [ ] ![Switch Pato Branco](sw-pb-nnn.txt)
- [ ] ![Switch Francisco Beltrão](sw-fb-nnn.txt)
- [ ] ![Switch Coronel Vivida](sw-cv-nnn.txt)


